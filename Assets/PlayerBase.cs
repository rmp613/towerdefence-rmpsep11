﻿using UnityEngine;
using System.Collections;

public class PlayerBase : BaseHealth {

	private int currentHealth;
	private int maxHealth;

	public GameObject healthUI;

	// Use this for initialization
	void Start () {
		currentHealth = 100;
		maxHealth = 100;
		//healthUI.GetComponent<HealthBarScript> ().Start (maxHealth);
	}

	public void SetHealth(GameObject health){
		healthUI = health;
		healthUI.GetComponent<HealthBarScript> ().maxHealth = 100;
	}

	
	// Update is called once per frame
	void Update () {
		if(healthUI != null)
			healthUI.GetComponent<HealthBarScript>().SetHealth (currentHealth);
	}

	public void TakeDamage(int amount){
		currentHealth -= amount;
	}

	public void GainHealth(int amount){
		currentHealth += amount;
	}
}
