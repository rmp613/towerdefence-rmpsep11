﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class TowerPanelScript : MonoBehaviour {

	public GameObject TowerName, TowerPrice;

	public void SetTowerPanel(Tower tower){
		if (tower == null) {
			TowerName.GetComponent<Text> ().text = "";
			TowerPrice.GetComponent<Text> ().text = "- $" + 0;
		}
		TowerName.GetComponent<Text> ().text = tower.type;
		TowerPrice.GetComponent<Text> ().text = "- $" + tower.price;
	}
}
