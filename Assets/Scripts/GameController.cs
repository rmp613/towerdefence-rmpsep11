﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class GameController : MonoBehaviour {
	public enum GameState { PHASE_BUILDING, PHASE_FIGHTING, PAUSED, WON, LOST };
	public GameState state = GameState.PHASE_BUILDING;
	bool firstPhase = true;

	public float buildTime = 90f;
	public float nextPhaseCountdown = 0;
	public float defaultNextPhaseCountdown = 1;

	GameObject towerPanel;

	//CURRENCY HERE TEMPORARILY AS NOT SURE WHAT NEEDS TO BE DONE TO MAKE IT WORK OVER LAN
	public int funds = 200;
	// Use this for initialization
	void Start () {
		towerPanel = GameObject.FindGameObjectWithTag ("TowerPanel");
	}
	// Update is called once per frame
	void Update () {
		UpdateUI ();
		//if its the first phase of the level and enter is pressed then transition into standard fight - build phases
		if (state == GameState.PHASE_BUILDING &&
			Input.GetKeyDown (KeyCode.Return) &&
			firstPhase) {
			nextPhaseCountdown = defaultNextPhaseCountdown;
			firstPhase = false;
		} else if (state == GameState.PHASE_BUILDING && !firstPhase){
			//if enter key is pressed in normal build phase then speed up timer by setting it to 5 seconds
			if (nextPhaseCountdown > 5 && Input.GetKeyDown (KeyCode.Return)) {
				nextPhaseCountdown = 5;
			}
			//keep the phase countdown ticking down
			if (nextPhaseCountdown > 0) {
				nextPhaseCountdown -= Time.deltaTime;
			} else if (nextPhaseCountdown <= 0) { //if there is no time on the countdown transition into fighting phase
				state = GameState.PHASE_FIGHTING;
			}
		}
	}
	void UpdateUI(){
		if (towerPanel == null) {
			towerPanel = GameObject.FindGameObjectWithTag ("TowerPanel");
		}
		//pretty self explanatory. just updates ui with current values
		if (GameObject.FindGameObjectsWithTag ("Player").Length > 0) { //this test just to make sure not in server choosing screen
			GameObject.FindGameObjectWithTag ("Funds").GetComponentInChildren<Text> ().text = "$ " + funds;
			if (state == GameState.PHASE_BUILDING) {
				GameObject.FindGameObjectWithTag ("Phase").GetComponentInChildren<Text> ().text = "Building for " +Mathf.RoundToInt(nextPhaseCountdown)+ " s";

				//towerPanel = GameObject.FindGameObjectWithTag ("TowerPanel");
				towerPanel.SetActive(true);

				//GameObject.FindGameObjectWithTag ("TowerPanel").SetActive (true);
			} else if (state == GameState.PHASE_FIGHTING) {
				GameObject.FindGameObjectWithTag ("Phase").GetComponentInChildren<Text> ().text = "Battle!";

				//towerPanel = GameObject.FindGameObjectWithTag ("TowerPanel");
				towerPanel.SetActive(false);


			}
			if (state == GameState.WON) {
				GameObject.FindGameObjectWithTag ("State").GetComponentInChildren<Text> ().text = "You Won!";
			} else if (state == GameState.LOST) {
				GameObject.FindGameObjectWithTag ("State").GetComponentInChildren<Text> ().text = "You Lost :(";
			}
		}
	}

}
