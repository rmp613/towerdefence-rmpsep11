﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BasicEnemy : BaseHealth {
	public float moveSpeed;//, maxHealth, health;
	protected Grid grid;
	//armor,regen,other special properties, targets,end bases,tower lists,	players, attack method, switch ai
	protected float weaponDamage, weaponRange;
	protected Pathfinder pathfinder;
	public enum targetStateEnum{targetBase,targetPlayer,targetTurret,attackingBase}
	protected targetStateEnum state = targetStateEnum.targetBase;
	protected int currentPathIndex = 0;
	protected int pathAheadCheck = 5;
	protected List<Vector2> path;
	public Vector3 moveTarget;
	protected List<GameObject> players;
	protected GameObject endBase;
	protected Vector3 playerPos;
	protected  int enemyId;
	protected  int maxHealth = 30; 
	protected  int currentHealth; 

	public int getCurrentHealth()
	{
		return currentHealth;
	}

	public float getCurrentHealthFloat()
	{
		return (float)currentHealth;
	}


	// Use this for initialization
	//@Override 
	public void Start() {
		players = new List<GameObject>(GameObject.FindGameObjectsWithTag ("Player"));
		grid = GameObject.Find("A*").GetComponent<Grid>();
		pathfinder = new Pathfinder(grid);
		endBase = GameObject.Find ("EndZone");
		playerPos = players [0].transform.position;
		updateState ();
		updatePath ();
		Physics.IgnoreCollision (GetComponent<BoxCollider> (), GameObject.FindGameObjectWithTag ("InWall").GetComponent<BoxCollider>());
	}

	// Update is called once per frame
	protected void Update () {
		updateState ();
		stateActions ();
		//if player moves or (add later if a tower is placed down) update path
		if (players [0] != null) {
			if (playerPos != players [0].transform.position) {
				updatePath ();
			}
			followPath ();
			playerPos = players [0].transform.position;
		}
	}
	protected void OnCollisionEnter(Collision collision){
		updatePath ();
	}
	//COULDNT GET THIS TO WORK AS THEY RAN INTO WALLS AND GOT STUCK SOMETIMES ALTHOUGHT THE OTHER CODE DOES THE SAME SO NOT SURE WHAT TO DO HERE
	//also if you want to try this and cant figure out the variables i havent included here i have a vs backed up so ask me and ill send u how i had it set up
/*	void LookAhead(){
		bool wayClear = true;
		lookIndex = currentPathIndex;
		RaycastHit hitObstacle;
		Vector3 prevTarget = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);;
		//while there are no obstacles between the current pathpoint and the pathpoint we are looking ahead to...
		while (wayClear) {
			//get movetarget
			moveTarget = grid.GetWorldPos((int)path[lookIndex].x, (int)path[lookIndex].y);
			moveTarget = new Vector3 (moveTarget.x, transform.position.y, moveTarget.z); //make movetarget.y the same as enemypos.y
			//setup the ray checking at foot level of enemies for obstacles ahead
			Vector3 rayOrigin = new Vector3 (transform.position.x, transform.position.y - height / 3, transform.position.z);
			Vector3 rayTarget = new Vector3 (moveTarget.x, transform.position.y - height / 3, moveTarget.z);
			Vector3 rayDir = rayTarget - rayOrigin;
			rayDir.Normalize ();

			//if obstacle ahead change movetarget to previous movetarget, change currentPathIndex to lookindex and return
			if (Physics.Raycast (rayOrigin, rayDir, out hitObstacle, Vector3.Distance (rayTarget, rayOrigin))) {
				moveTarget = prevTarget;
				currentPathIndex = lookIndex-1;
				wayClear = false;
				return;
			}


			//else increment lookindex and set prevTarget
			lookIndex++;
			prevTarget = moveTarget;
		}
	}*/
	protected void updateState(){
		if (state != targetStateEnum.targetPlayer) {
			if (state != targetStateEnum.attackingBase && Vector3.Distance (endBase.transform.position, transform.position) < weaponRange) {
				state = targetStateEnum.attackingBase;
			} else {
				state = targetStateEnum.targetBase;
			}
		}
	}
	protected void stateActions(){
		switch (state) {
		case targetStateEnum.targetBase:
			break;
		case targetStateEnum.attackingBase:
			
			Explode ();

			break;
		}
	}

	protected void Explode() {
		if(!GetComponent<ParticleSystem>().isPlaying){
			GameObject.FindGameObjectWithTag("EndZone").GetComponent<PlayerBase>().RpcTakeDamage(1);
		}
		var exp = GetComponent<ParticleSystem>();
		exp.Play();
		Destroy(gameObject, exp.duration);

		//anything else?
		//checking if it works with wave
		//how do i do a wave?


	}
	public void setAttributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e){
		setAttributes (moveSpeed, health, weaponDamage, weaponRange, e, 1);
	} 
	public void setAttributes(float moveSpeed, float health, float weaponDamage, float weaponRange, targetStateEnum e, int enemyID){
		this.weaponRange = weaponRange;
		this.moveSpeed = moveSpeed;
		currentHealth = maxHealth = (int)health; //TODO make sure this is passing right
		this.weaponDamage = weaponDamage;
		state = e;
		this.enemyId = enemyId;
		Start ();
		updatePath();
		calculateMoveTarget();
	} 





	/*
	//when this enemy takes damage returns true if it died 
	public bool RemoveHealth(int hitAmount){
		health -= hitAmount;
		if(health <= 0){
			deathActions();
			return true;
		}
		return false;
	}
	private void deathActions(){
		//stuff

		Destroy (gameObject);
	}
	*/




	protected void followPath(){
		if(path != null && currentPathIndex < path.Count){
			if (!checkPathClear ()) {
				updatePath ();
			}
			if(moveTarget != transform.position){
				calculateMoveTarget();
			}
			else{
				currentPathIndex++;
				calculateMoveTarget();
			}
			float step = moveSpeed * Time.deltaTime;
			transform.position = Vector3.MoveTowards(transform.position, moveTarget, step);
		}
	}
	protected  void calculateMoveTarget(){
		moveTarget = grid.GetWorldPos((int)path[currentPathIndex].x, (int)path[currentPathIndex].y);

		moveTarget = new Vector3(moveTarget.x, transform.position.y, moveTarget.z);
	}
	protected  bool checkPathClear(){
		for (int i = 0; currentPathIndex + i < path.Count && i < pathAheadCheck; i++) {
			if(!pathfinder.checkWalkable((int)path[currentPathIndex + i].x, (int)path[currentPathIndex + i].y)){
				return false;
			}
		}
		return true;
	}
	public void updatePath(){
		switch(state){
		case targetStateEnum.targetPlayer:
			updatePathP (players);
			break;
		case targetStateEnum.targetBase:
		default:
			updatePath (endBase);
			break;
		}
	}
	protected  void updatePath(GameObject target){
		path = pathfinder.findPath (grid.GetVector2 (transform.position), grid.GetVector2 (target.transform.position));
		currentPathIndex = 0;
	}
	//calculate best player to target
	//overide this method for smarter behievour
	protected void updatePathP(List<GameObject> targets){
		updatePath (targets [0]);
	}
}
