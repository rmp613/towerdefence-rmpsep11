﻿using UnityEngine;
using System.Collections;

public class EnemyType{
	public string name;
	public BasicEnemy.targetStateEnum target;
	public GameObject prefab;
	public int amount = 1;
	public EnemyType(string name, BasicEnemy.targetStateEnum target, GameObject prefab){
		this.name = name;
		this.target = target;
		this.prefab = prefab;
	}
	public EnemyType(string name, BasicEnemy.targetStateEnum target, GameObject prefab, int amount){
		this.name = name;
		this.target = target;
		this.prefab = prefab;
		this.amount = amount;
	}
}

