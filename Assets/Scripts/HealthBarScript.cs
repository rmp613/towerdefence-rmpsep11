﻿using UnityEngine;
using System.Collections;
using UnityEditor;


public class HealthBarScript : MonoBehaviour {

	public GameObject bar;
	public int maxHealth;
	float width;
	float step;
	RectTransform transform;
	// Use this for initialization

	/*void Start(){
		Start(100);
	}*/

	void Start () {
		transform = bar.GetComponent<RectTransform> ();
		width = transform.rect.width;
		step = width / maxHealth;  //Shouldn't be hardcoded to 100 if generic
		transform.sizeDelta = new Vector2 (step * maxHealth, transform.sizeDelta.y);
	}
	
	// Update is called once per frame
	public void SetHealth (float healthValue) {
		transform.sizeDelta = new Vector2 (healthValue * step, transform.sizeDelta.y);
		//Test
		//if (transform.sizeDelta.x > 0) {
		//	Statistics.playerHealth -= 1;
		//}

	}
}
