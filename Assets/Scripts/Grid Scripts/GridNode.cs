﻿using UnityEngine;
using System.Collections;

public class GridNode {
	public bool walkable;
	public Vector3 worldPos;

	public GridNode(bool walkable, Vector3 worldPos){
		this.walkable = walkable;
		this.worldPos = worldPos;
	}
}
